package assert

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
	"testing"

	"github.com/gorilla/sessions"
)

// StatusCode fails if the status code of `resp` is not equal to `code`.
func StatusCode(t *testing.T, resp *http.Response, code int) {
	if resp.StatusCode != code {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("%s:%d: expected status code to be %d but got %d\n", filepath.Base(file), line, code, resp.StatusCode)
		t.FailNow()
	}
}

// ContentType fails if the content type header of `resp` is not equal to `expected`. "charset=utf-8" is ignored.
func ContentType(t *testing.T, resp *http.Response, expected string) {
	if contentType := strings.Split(resp.Header.Get("Content-Type"), ";")[0]; contentType != expected { // ignore "charset=utf-8"
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("%s:%d: expected content type to be %s but got %s\n", filepath.Base(file), line, expected, contentType)
		t.FailNow()
	}
}

// Content fails if the trimmed content is not equal to `expected`.
func Content(t *testing.T, resp *http.Response, expected string) {
	body, _ := ioutil.ReadAll(resp.Body)
	actual := strings.TrimSpace(string(body))
	if actual != expected {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("%s:%d:\nexpected:\n%v\nbut got:\n%v\n", filepath.Base(file), line, expected, actual)
		t.FailNow()
	}
}

// ContentContains fails if the content does not contain `expected`.
func ContentContains(t *testing.T, resp *http.Response, expected string) {
	body, _ := ioutil.ReadAll(resp.Body)
	actual := strings.TrimSpace(string(body))

	match, _ := regexp.MatchString(expected, actual)
	if !match {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("%s:%d:\nexpected content to contain\n%s\nbut didn't:\n%s\n", filepath.Base(file), line, expected, actual)
		t.FailNow()
	}
}

// UserIDInSession fails if the given user.ID is not in the session.
func UserIDInSession(t *testing.T, req *http.Request, store *sessions.CookieStore, sessionName, userIDKey, expected string) {
	session, _ := store.Get(req, sessionName)
	actual := session.Values[userIDKey]
	if actual != expected {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("%s:%d:\nexpected user.ID in session to be %s but got %s\n", filepath.Base(file), line, expected, actual)
		t.FailNow()
	}
}

// UserIDNotInSession fails if a user.ID is in the session.
func UserIDNotInSession(t *testing.T, req *http.Request, store *sessions.CookieStore, sessionName, userIDKey string) {
	session, _ := store.Get(req, sessionName)
	sessionUserID := session.Values[userIDKey]
	if _, ok := sessionUserID.(string); ok {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("%s:%d:\nexpected no user.ID in session but got %s\n", filepath.Base(file), line, sessionUserID)
		t.FailNow()
	}
}
