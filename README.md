# auth-static

> Protect static files to be accessible to logged-in users only.

The files are served by a web server that supports the `X-Accel-Redirect` HTTP header.
Authentication is handled by a web app written in Go.

The following parts are included:
- command line tools:
  - to initialise a SQLite database
  - to add a new user
  - to reset the password and generate a new code of an existing user
  - to generate cookie security keys
- a web app with handlers:
  - `signup` that allows new users to set their password
  - `signin` that allows users to log in
  - `authentication` that ensures the user is logged in and that tells the web server to serve the requested static file

## Prerequisites

- [Caddy](https://caddyserver.com/) (for alternatives see [Web server](#web-server))
- [Go](https://golang.org/) (tested with 1.12.4)

## Install

Clone this repo, run the tests, dependencies are installed automatically:

    $ cd /tmp/
    $ git clone https://gitlab.com/kschaper/auth-static^
    $ cd auth-static^/
    $ go test ./...

Install the binaries. The output dir specified by `-o` has to be in your `$PATH`.

    $ go build -o ~/bin/as-initdb ./cmd/initdb/ \
      && go build -o ~/bin/as-createuser ./cmd/createuser/ \
      && go build -o ~/bin/as-resetpassword ./cmd/resetpassword/ \
      && go build -o ~/bin/as-web ./cmd/web/ \
      && go build -o ~/bin/as-genkey ./cmd/genkey/

## Example

Change into the `example` directory.

Start the web server:

    $ caddy

Create the database, in another shell:

    $ as-initdb

This creates a `dev.db` in the current working directory.
To put it somewhere else or to specify a different name use the `-dsn` flag:

    $ as-initdb -dsn "/path/to/my.db"

See go-sqlite3's [SQLiteDriver.Open](https://godoc.org/github.com/mattn/go-sqlite3#SQLiteDriver.Open) for accepted values.

Start the app:

    $ as-web

If a `.env` file exists in the current working directory it will be read to get the configuration values.
Otherwise the following default values will be used:

~~~
AS_HOST=localhost  # the host the app listens to
AS_PORT=9000       # the port the app listens to

# database
AS_DSN=dev.db # data source name

# cookie
AS_HASHKEY=<generated>      # authentication key (use as-genkey, see below)
AS_BLOCKKEY=<generated>     # encryption key (use as-genkey, see below)
AS_KEYLENGTH=32             # length for both keys: len(AS_HASHKEY) == len(AS_BLOCKKEY) == AS_KEYLENGTH
AS_SECURE=false             # secure flag (should be true when using HTTPS)
AS_SESSION_NAME=auth-static # session name
AS_USER_ID_KEY=user_id      # user ID session key

# paths
AS_EXTERNAL=/private/  # protected area external dir
AS_INTERNAL=/internal/ # protected area internal dir
AS_HOME=main.html      # protected area home
~~~

To specify another file use the `-env` flag, for example:

    $ as-web -env /path/to/.env.prod

To generate cookie security keys `as-genkey` can be used:

    $ as-genkey
    8cb...
    $ as-genkey
    3cf...

If no keys are provided then they will be generated on app start.
This is for development purposes only.
They will be lost on shut down.

Create the first user, in another shell:

    $ as-createuser -email me@example.com
    successfully created user with email "me@example.com" and code "e80ef0a04db3597e09fee4e958ca12b1"

_Note: use the `-dsn` flag if the database file is not `dev.db` in the current working directory._

Use the code to create a URL:

    http://localhost:8080/signup/e80ef0a04db3597e09fee4e958ca12b1

That's the URL to initially set a password. After that the user will be redirected to the protected area.
Users can sign in on http://localhost:8080/signin.
http://localhost:8080/ is public.
Everything in the `internal` directory is protected and accessible only to authenticated requests via `private` URL path: http://localhost:8080/private/main.html.

## Web server

Any webserver that supports the `X-Accel-Redirect` or `X-Sendfile` HTTP headers can be used. For example:

- [http.internal](https://caddyserver.com/docs/internal) - Caddy, see `./example/Caddyfile` for example configuration.
- [X-Accel](https://www.nginx.com/resources/wiki/start/topics/examples/x-accel/) and [XSendfile](https://www.nginx.com/resources/wiki/start/topics/examples/xsendfile/) - nginx
- [mod_xsendfile](https://tn123.org/mod_xsendfile/) - Apache

## Development

Run the tests:

    $ go test ./...

## Maintenance

Update Go:

- "Prerequisites" in `README.md`
- "image" in `.gitlab-ci.yml`
