package main

import (
	"fmt"

	"gitlab.com/kschaper/auth-static/config"
)

func main() {
	fmt.Printf("%s\n", config.GenerateKey())
}
