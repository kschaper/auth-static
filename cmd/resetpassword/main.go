package main

import (
	"flag"
	"fmt"
	"os"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/kschaper/auth-static/services"
)

var (
	email = flag.String("email", "", "email of new user")
	dsn   = flag.String("dsn", "dev.db", "data source name")
)

func init() {
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
	}
}

func main() {
	flag.Parse()

	if *email == "" {
		fmt.Println("error: no email given")
		flag.Usage()
		os.Exit(1)
	}

	if *dsn == "" {
		fmt.Println("error: no dsn given")
		flag.Usage()
		os.Exit(1)
	}

	client := &services.DatabaseClient{DSN: *dsn}
	db, err := client.Open()
	if err != nil {
		fmt.Printf("error: %s\n", err)
		os.Exit(1)
	}

	userService := &services.UserService{DB: db}
	code, err := userService.Reset(*email)
	if err != nil {
		fmt.Printf("error: %s\n", err)
		os.Exit(1)
	}

	fmt.Printf("successfully saved user with email %q and code %q\n", *email, code)
}
