package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/kschaper/auth-static/handler"

	"github.com/gobuffalo/envy"
	_ "github.com/mattn/go-sqlite3"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"gitlab.com/kschaper/auth-static/config"
	"gitlab.com/kschaper/auth-static/services"
)

// config values either from .env file or defaults
var (
	host        string // localhost
	port        int    // 9000
	dsn         string // dev.db
	hashKey     string // generated
	blockKey    string // generated
	keyLength   int    // 32
	secure      bool   // false
	sessionName string // auth-static
	userIDKey   string // user_id
	external    string // /private/
	internal    string // /internal/
	home        string // main.html
)

func init() {
	// read .env file
	env := flag.String("env", "", "path to .env file")
	flag.Parse()

	if *env != "" {
		if err := envy.Load(*env); err != nil {
			panic(fmt.Sprintf("could not open %s", *env))
		}
	}

	// assign values without conversion
	host = envy.Get("AS_HOST", "localhost")
	dsn = envy.Get("AS_DSN", "dev.db")
	hashKey = envy.Get("AS_HASHKEY", config.GenerateKey())
	blockKey = envy.Get("AS_BLOCKKEY", config.GenerateKey())
	sessionName = envy.Get("AS_SESSION_NAME", "auth-static")
	userIDKey = envy.Get("AS_USER_ID_KEY", "user_id")
	external = envy.Get("AS_EXTERNAL", "/private/")
	internal = envy.Get("AS_INTERNAL", "/internal/")
	home = envy.Get("AS_HOME", "main.html")

	// assign values with conversion
	var err error

	envPort := envy.Get("AS_PORT", "9000")
	port, err = strconv.Atoi(envPort)
	if err != nil {
		panic(fmt.Sprintf("could not convert AS_PORT %s into integer value", envPort))
	}

	envKeyLength := envy.Get("AS_KEYLENGTH", "32")
	keyLength, err = strconv.Atoi(envKeyLength)
	if err != nil {
		panic(fmt.Sprintf("could not convert AS_KEYLENGTH %s into integer value", envKeyLength))
	}

	envSecure := envy.Get("AS_SECURE", "false")
	secure, err = strconv.ParseBool(envSecure)
	if err != nil {
		panic(fmt.Sprintf("could not convert AS_SECURE %s into boolean value", envSecure))
	}

	// validate values
	if len(hashKey) != keyLength || len(blockKey) != keyLength {
		panic(fmt.Sprintf("please provide AS_HASHKEY and AS_BLOCKKEY both with AS_KEYLENGTH %d chars", keyLength))
	}
	if sessionName == "" {
		panic("please provide AS_SESSION_NAME")
	}
	if userIDKey == "" {
		panic("please provide AS_USER_ID_KEY")
	}
}

func main() {
	// database client
	client := services.DatabaseClient{DSN: dsn}
	db, err := client.Open()
	if err != nil {
		panic(err)
	}

	// session
	store := sessions.NewCookieStore([]byte(hashKey), []byte(blockKey))
	store.Options = &sessions.Options{
		Path:     "/",
		HttpOnly: true,
		Secure:   secure,
		SameSite: http.SameSiteStrictMode,
	}

	// services
	userService := &services.UserService{DB: db}

	// config used in handlers
	conf := &config.Config{
		SessionName:              sessionName,
		UserIDKey:                userIDKey,
		ProtectedAreaDirExternal: external,
		ProtectedAreaDirInternal: internal,
		ProtectedAreaHome:        home,
	}

	// env
	env := &handler.Env{
		Conf:        conf,
		Store:       store,
		UserService: userService,
	}

	// routes
	r := mux.NewRouter()
	r.Handle("/signup/{code:[a-z0-9]{32}}", handler.Handler{Env: env, Action: handler.SignupFormAction}).Methods("GET")
	r.Handle("/signup/{code:[a-z0-9]{32}}", handler.Handler{Env: env, Action: handler.SignupAction}).Methods("POST")
	r.Handle("/signin", handler.Handler{Env: env, Action: handler.SigninFormAction}).Methods("GET")
	r.Handle("/signin", handler.Handler{Env: env, Action: handler.SigninAction}).Methods("POST")
	r.Handle("/signout", handler.Handler{Env: env, Action: handler.SignoutAction}).Methods("POST")
	r.PathPrefix(conf.ProtectedAreaDirExternal).Handler(handler.Handler{Env: env, Action: handler.AuthenticationAction})
	http.Handle("/", r)

	// server
	addr := fmt.Sprintf("%s:%d", host, port)
	fmt.Printf("Server running at http://%s\n", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}
