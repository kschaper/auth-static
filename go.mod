module gitlab.com/kschaper/auth-static

go 1.12

require (
	github.com/gobuffalo/envy v1.7.0
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.7.1
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.1.3
	github.com/joho/godotenv v1.3.0
	github.com/konsorten/go-windows-terminal-sequences v1.0.1
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/pkg/errors v0.8.1
	github.com/rogpeppe/go-internal v1.3.0
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.4.1
	golang.org/x/crypto v0.0.0-20190422183909-d864b10871cd
	golang.org/x/sys v0.0.0-20190412213103-97732733099d
)
