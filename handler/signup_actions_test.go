package handler_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"regexp"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/kschaper/auth-static/assert"
	"gitlab.com/kschaper/auth-static/handler"
	"gitlab.com/kschaper/auth-static/services"
)

func TestSignupFormAction(t *testing.T) {
	cases := map[string]func(t *testing.T){
		"success": func(t *testing.T) {
			var (
				env  = &handler.Env{Conf: testConfig(), Store: sessions.NewCookieStore([]byte("abc"))}
				h    = handler.Handler{Env: env, Action: handler.SignupFormAction}
				code = "73d3e3502ab73f40d4943fdcc16d05dd"
			)

			// server
			router := mux.NewRouter()
			router.Handle("/signup/{code:[a-z0-9]{32}}", h).Methods("GET")
			ts := httptest.NewServer(router)
			defer ts.Close()

			// request
			resp := getRequest(t, ts.URL+"/signup/"+code)
			defer resp.Body.Close()

			assert.StatusCode(t, resp, http.StatusOK)
			assert.ContentContains(t, resp, fmt.Sprintf(`action="/signup/%s"`, code))
		},
		"with flash messages": func(t *testing.T) {
			var (
				conf  = testConfig()
				store = sessions.NewCookieStore([]byte("abc"))
				env   = &handler.Env{Conf: conf, Store: store}
				h     = handler.Handler{Env: env, Action: handler.SignupFormAction}
				w     = httptest.NewRecorder()
			)

			// request
			req, err := http.NewRequest("GET", "/signin", nil)
			if err != nil {
				t.Fatal(err)
			}

			// put flash messages in session
			session, _ := store.Get(req, conf.SessionName)
			session.AddFlash("foo")
			session.AddFlash("bar")
			if err := session.Save(req, w); err != nil {
				t.Fatal(err)
			}

			// invoke handler
			h.ServeHTTP(w, req)

			// ensure flash messages are rendered
			r := regexp.MustCompile("(?s)foo.*bar")
			if !r.MatchString(w.Body.String()) {
				t.Fatalf("expected content to contain\n%s\nbut didn't:\n%s\n", "foo.*bar", w.Body.String())
			}
		},
	}

	for n, c := range cases {
		t.Run(n, c)
	}
}

func TestSignupAction(t *testing.T) {
	cases := map[string]func(t *testing.T){
		"success": func(t *testing.T) {
			var (
				conf        = testConfig()
				db          = db(t)
				userService = &services.UserService{DB: db}
				authKey     = []byte("1234567890abcdef")
				store       = sessions.NewCookieStore(authKey)
				env         = &handler.Env{Conf: conf, Store: store, UserService: userService}
				h           = handler.Handler{Env: env, Action: handler.SignupAction}
				email       = "webmaster@example.com"
				password    = strings.Repeat("k", services.PasswordMinLen)
				code        = createUser(t, userService, email)
			)

			// get the user.ID by code
			// TODO: 1. introduce User.Code 2. let UserService.Create return a User 3. remove GetIDByCode
			id, err := userService.GetIDByCode(code)
			if err != nil {
				t.Fatal(err)
			}

			// request
			data := url.Values{"password": {password}, "confirmation": {password}}
			req, err := http.NewRequest("POST", "/signup/"+code, strings.NewReader(data.Encode()))
			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
			if err != nil {
				t.Fatal(err)
			}

			w := httptest.NewRecorder()
			router := mux.NewRouter()
			router.Handle("/signup/{code:[a-z0-9]{32}}", h).Methods("POST")

			// invoke handler
			router.ServeHTTP(w, req)

			assert.StatusCode(t, w.Result(), http.StatusFound)

			// ensure redirect to protected area
			location := w.Result().Header.Get("Location")
			expectedLocation := conf.ProtectedAreaDirExternal + conf.ProtectedAreaHome
			if location != expectedLocation {
				t.Fatalf("expected redirect to %s but was to %s\n", expectedLocation, location)
			}

			// get session
			session := make(map[interface{}]interface{})
			s := securecookie.New(authKey, nil)
			if err := s.Decode(conf.SessionName, w.Result().Cookies()[0].Value, &session); err != nil {
				t.Fatal(err)
			}

			// compare user.ID from session with ID from db
			if session[conf.UserIDKey].(string) != id.String() {
				t.Fatalf("expected ID in session to be %s but got %s", id, session[conf.UserIDKey])
			}
		},
		"unknown code": func(t *testing.T) {
			var (
				conf        = testConfig()
				db          = db(t)
				userService = &services.UserService{DB: db}
				authKey     = []byte("1234567890abcdef")
				store       = sessions.NewCookieStore(authKey)
				env         = &handler.Env{Conf: conf, Store: store, UserService: userService}
				h           = handler.Handler{Env: env, Action: handler.SignupAction}
				password    = strings.Repeat("k", services.PasswordMinLen)
				code        = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
			)

			// request
			data := url.Values{"password": {password}, "confirmation": {password}}
			req, err := http.NewRequest("POST", "/signup/"+code, strings.NewReader(data.Encode()))
			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
			if err != nil {
				t.Fatal(err)
			}

			w := httptest.NewRecorder()
			router := mux.NewRouter()
			router.Handle("/signup/{code:[a-z0-9]{32}}", h).Methods("POST")

			// invoke handler
			router.ServeHTTP(w, req)

			assert.StatusCode(t, w.Result(), http.StatusFound)

			// ensure redirect to signup page again
			location := w.Result().Header.Get("Location")
			expectedLocation := "/signup/" + code
			if location != expectedLocation {
				t.Fatalf("expected redirect to %s but was to %s\n", expectedLocation, location)
			}

			// get session
			session := make(map[interface{}]interface{})
			s := securecookie.New(authKey, nil)
			if err := s.Decode(conf.SessionName, w.Result().Cookies()[0].Value, &session); err != nil {
				t.Fatal(err)
			}

			// ensure no user.ID is in the session
			if session[conf.UserIDKey] != nil {
				t.Fatalf("expected no ID in session but got %s", session[conf.UserIDKey])
			}
		},
		"password update failed": func(t *testing.T) {
			var (
				conf        = testConfig()
				db          = db(t)
				userService = &services.UserService{DB: db}
				authKey     = []byte("1234567890abcdef")
				store       = sessions.NewCookieStore(authKey)
				env         = &handler.Env{Conf: conf, Store: store, UserService: userService}
				h           = handler.Handler{Env: env, Action: handler.SignupAction}
				email       = "webmaster@example.com"
				password    = strings.Repeat("k", services.PasswordMinLen-1)
				code        = createUser(t, userService, email)
			)

			// request
			data := url.Values{"password": {password}, "confirmation": {password}}
			req, err := http.NewRequest("POST", "/signup/"+code, strings.NewReader(data.Encode()))
			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
			if err != nil {
				t.Fatal(err)
			}

			w := httptest.NewRecorder()
			router := mux.NewRouter()
			router.Handle("/signup/{code:[a-z0-9]{32}}", h).Methods("POST")

			// invoke handler
			router.ServeHTTP(w, req)

			assert.StatusCode(t, w.Result(), http.StatusFound)

			// ensure redirect to signup page again
			location := w.Result().Header.Get("Location")
			expectedLocation := "/signup/" + code
			if location != expectedLocation {
				t.Fatalf("expected redirect to %s but was to %s\n", expectedLocation, location)
			}

			// get session
			session := make(map[interface{}]interface{})
			s := securecookie.New(authKey, nil)
			if err := s.Decode(conf.SessionName, w.Result().Cookies()[0].Value, &session); err != nil {
				t.Fatal(err)
			}

			// ensure no user.ID is in the session
			if session[conf.UserIDKey] != nil {
				t.Fatalf("expected no ID in session but got %s", session[conf.UserIDKey])
			}
		},
	}

	for n, c := range cases {
		t.Run(n, c)
	}
}
