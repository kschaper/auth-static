package handler_test

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/sessions"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/kschaper/auth-static/handler"
	"gitlab.com/kschaper/auth-static/services"
)

func TestAuthenticationAction(t *testing.T) {
	cases := map[string]func(t *testing.T){
		"authenticated": func(t *testing.T) {
			var (
				db           = db(t)
				userService  = &services.UserService{DB: db}
				id           = createSignedUpUser(t, userService, "webmaster@example.com", strings.Repeat("k", services.PasswordMinLen))
				conf         = testConfig()
				store        = sessions.NewCookieStore([]byte("abc"))
				env          = &handler.Env{Conf: conf, Store: store, UserService: userService}
				h            = handler.Handler{Env: env, Action: handler.AuthenticationAction}
				w            = httptest.NewRecorder()
				requestPath  = "/private/secret.jpg"
				redirectPath = "/internal/secret.jpg"
				contentType  = "image/jpeg"
			)

			// request
			req, err := http.NewRequest("GET", requestPath, nil)
			if err != nil {
				t.Fatal(err)
			}

			// put the user id in session
			session, _ := store.Get(req, conf.SessionName)
			session.Values[conf.UserIDKey] = id
			if err := session.Save(req, w); err != nil {
				t.Fatal(err)
			}

			// set the session cookie for the request
			req.Header.Set("Set-Cookie", w.Result().Cookies()[0].Value)

			// invoke handler
			h.ServeHTTP(w, req)

			// ensure status code 200
			if w.Code != http.StatusOK {
				t.Fatalf("expected status code %d but got %d\n", http.StatusOK, w.Code)
			}

			// ensure X-Accel-Redirect header has correct value
			redirectHeader := w.Header().Get("X-Accel-Redirect")
			if redirectHeader != redirectPath {
				t.Fatalf("expected X-Accel-Redirect with path %q but got %q\n", redirectPath, redirectHeader)
			}

			// ensure Content-Type header is correct
			contentTypeHeader := w.Header().Get("Content-Type")
			if contentTypeHeader != contentType {
				t.Fatalf("expected Content-Type with %q but got %q\n", contentType, contentTypeHeader)
			}
		},
		"unauthenticated": func(t *testing.T) {
			var (
				db          = db(t)
				userService = &services.UserService{DB: db}
				conf        = testConfig()
				store       = sessions.NewCookieStore([]byte("abc"))
				env         = &handler.Env{Conf: conf, Store: store, UserService: userService}
				h           = handler.Handler{Env: env, Action: handler.AuthenticationAction}
				w           = httptest.NewRecorder()
			)

			// request
			req, err := http.NewRequest("GET", "/private/whatever.html", nil)
			if err != nil {
				t.Fatal(err)
			}

			// get the session
			session, _ := store.Get(req, conf.SessionName)

			// put unkown user id in session
			session.Values[conf.UserIDKey] = uuid.NewV4().String()
			if err := session.Save(req, w); err != nil {
				t.Fatal(err)
			}

			// set the session cookie for the request
			req.Header.Set("Set-Cookie", w.Result().Cookies()[0].Value)

			// invoke handler
			h.ServeHTTP(w, req)

			// ensure status code 404
			if w.Code != http.StatusNotFound {
				t.Fatalf("expected status code %d but got %d\n", http.StatusNotFound, w.Code)
			}

			// ensure X-Accel-Redirect header is not set
			redirectHeader := w.Header().Get("X-Accel-Redirect")
			if redirectHeader != "" {
				t.Fatalf("expected X-Accel-Redirect not to be set but got value %q\n", redirectHeader)
			}
		},
		"without file extension in path": func(t *testing.T) {
			var (
				db           = db(t)
				userService  = &services.UserService{DB: db}
				id           = createSignedUpUser(t, userService, "webmaster@example.com", strings.Repeat("k", services.PasswordMinLen))
				conf         = testConfig()
				store        = sessions.NewCookieStore([]byte("abc"))
				env          = &handler.Env{Conf: conf, Store: store, UserService: userService}
				h            = handler.Handler{Env: env, Action: handler.AuthenticationAction}
				w            = httptest.NewRecorder()
				requestPath  = "/private/secret/"
				redirectPath = "/internal/secret/"
				contentType  = "text/html"
			)

			// request
			req, err := http.NewRequest("GET", requestPath, nil)
			if err != nil {
				t.Fatal(err)
			}

			// put the user id in session
			session, _ := store.Get(req, conf.SessionName)
			session.Values[conf.UserIDKey] = id
			if err := session.Save(req, w); err != nil {
				t.Fatal(err)
			}

			// set the session cookie for the request
			req.Header.Set("Set-Cookie", w.Result().Cookies()[0].Value)

			// invoke handler
			h.ServeHTTP(w, req)

			// ensure status code 200
			if w.Code != http.StatusOK {
				t.Fatalf("expected status code %d but got %d\n", http.StatusOK, w.Code)
			}

			// ensure X-Accel-Redirect header has correct value
			redirectHeader := w.Header().Get("X-Accel-Redirect")
			if redirectHeader != redirectPath {
				t.Fatalf("expected X-Accel-Redirect with path %q but got %q\n", redirectPath, redirectHeader)
			}

			// ensure Content-Type header is correct
			contentTypeHeader := w.Header().Get("Content-Type")
			if contentTypeHeader != contentType {
				t.Fatalf("expected Content-Type with %q but got %q\n", contentType, contentTypeHeader)
			}
		},
	}

	for n, c := range cases {
		t.Run(n, c)
	}
}
