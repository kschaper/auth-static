package handler_test

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"regexp"
	"strings"
	"testing"

	"gitlab.com/kschaper/auth-static/assert"

	"github.com/gorilla/sessions"
	"gitlab.com/kschaper/auth-static/handler"
	"gitlab.com/kschaper/auth-static/services"
)

func TestSigninFormAction(t *testing.T) {
	cases := map[string]func(t *testing.T){
		"success": func(t *testing.T) {
			var (
				env = &handler.Env{Conf: testConfig(), Store: sessions.NewCookieStore([]byte("abc"))}
				h   = handler.Handler{Env: env, Action: handler.SigninFormAction}
				w   = httptest.NewRecorder()
			)

			// request
			req, err := http.NewRequest("GET", "/signin", nil)
			if err != nil {
				t.Fatal(err)
			}

			// invoke handler
			h.ServeHTTP(w, req)

			// ensure status code is correct
			if w.Code != http.StatusOK {
				t.Fatalf("expected status code to be %d but got %d", http.StatusOK, w.Code)
			}

			actual := w.Body.String()
			expected := `action="/signin"`
			if !strings.Contains(actual, expected) {
				t.Fatalf("expected content to contain\n%s\nbut didn't:\n%s\n", expected, actual)
			}
		},
		"with flash messages": func(t *testing.T) {
			var (
				conf  = testConfig()
				store = sessions.NewCookieStore([]byte("abc"))
				env   = &handler.Env{Conf: conf, Store: store}
				h     = handler.Handler{Env: env, Action: handler.SigninFormAction}
				w     = httptest.NewRecorder()
			)

			// request
			req, err := http.NewRequest("GET", "/signin", nil)
			if err != nil {
				t.Fatal(err)
			}

			// put flash messages in session
			session, _ := store.Get(req, conf.SessionName)
			session.AddFlash("foo")
			session.AddFlash("bar")
			if err := session.Save(req, w); err != nil {
				t.Fatal(err)
			}

			// invoke handler
			h.ServeHTTP(w, req)

			r := regexp.MustCompile("(?s)foo.*bar")
			if !r.MatchString(w.Body.String()) {
				t.Fatalf("expected content to contain\n%s\nbut didn't:\n%s\n", "foo.*bar", w.Body.String())
			}
		},
	}

	for n, c := range cases {
		t.Run(n, c)
	}
}

func TestSigninAction(t *testing.T) {
	cases := map[string]func(t *testing.T){
		"success": func(t *testing.T) {
			var (
				conf        = testConfig()
				userService = &services.UserService{DB: db(t)}
				store       = sessions.NewCookieStore([]byte("abc"))
				env         = &handler.Env{Conf: conf, Store: store, UserService: userService}
				h           = handler.Handler{Env: env, Action: handler.SigninAction}
				w           = httptest.NewRecorder()
				email       = "webmaster@example.com"
				password    = strings.Repeat("k", services.PasswordMinLen)
				id          = createSignedUpUser(t, userService, email, password)
			)

			// request
			data := url.Values{"email": {email}, "password": {password}}
			req, err := http.NewRequest("POST", "/signin", strings.NewReader(data.Encode()))
			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
			if err != nil {
				t.Fatal(err)
			}

			// invoke handler
			h.ServeHTTP(w, req)

			// ensure status code is correct
			if w.Code != http.StatusFound {
				t.Fatalf("expected status code to be %d but got %d", http.StatusFound, w.Code)
			}

			// ensure redirect to protected area
			location := w.Header().Get("Location")
			expectedLocation := conf.ProtectedAreaDirExternal + conf.ProtectedAreaHome
			if location != expectedLocation {
				t.Fatalf("expected redirect to %s but was to %s\n", expectedLocation, location)
			}

			// ensure user.ID is in session
			assert.UserIDInSession(t, req, store, conf.SessionName, conf.UserIDKey, id)
		},
		"failure": func(t *testing.T) {
			var (
				conf        = testConfig()
				userService = &services.UserService{DB: db(t)}
				store       = sessions.NewCookieStore([]byte("abc"))
				env         = &handler.Env{Conf: conf, Store: store, UserService: userService}
				h           = handler.Handler{Env: env, Action: handler.SigninAction}
				w           = httptest.NewRecorder()
			)

			// request
			data := url.Values{"email": {"unknown@example.com"}, "password": {strings.Repeat("k", services.PasswordMinLen)}}
			req, err := http.NewRequest("POST", "/signin", strings.NewReader(data.Encode()))
			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
			if err != nil {
				t.Fatal(err)
			}

			// invoke handler
			h.ServeHTTP(w, req)

			// ensure status code is correct
			if w.Code != http.StatusFound {
				t.Fatalf("expected status code to be %d but got %d", http.StatusFound, w.Code)
			}

			// ensure redirect to sign-in page
			location := w.Header().Get("Location")
			expectedLocation := "/signin"
			if location != expectedLocation {
				t.Fatalf("expected redirect to %s but was to %s\n", expectedLocation, location)
			}

			// ensure user.ID is not in session
			assert.UserIDNotInSession(t, req, store, conf.SessionName, conf.UserIDKey)

			// ensure there are flash messages
			session, _ := store.Get(req, conf.SessionName)
			lenFlashes := len(session.Flashes())
			if lenFlashes == 0 {
				t.Fatal("expected there to be flash messages")
			}
		},
	}

	for n, c := range cases {
		t.Run(n, c)
	}
}
