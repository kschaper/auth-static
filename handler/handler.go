package handler

import (
	"fmt"
	"net/http"

	"github.com/gorilla/sessions"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/kschaper/auth-static/config"
	"gitlab.com/kschaper/auth-static/services"
)

// Env provides access to commonly needed things.
type Env struct {
	Conf        *config.Config
	Store       *sessions.CookieStore
	UserService *services.UserService
}

// ErrorNotFound is returned when authentication failed.
type ErrorNotFound struct {
	Err       error // original error
	LogMsg    string
	LogFields log.Fields
}

// Error returns the error's text which is "Not Found".
func (e ErrorNotFound) Error() string { return http.StatusText(http.StatusNotFound) }

// Status returns the error's code which is "404".
func (e ErrorNotFound) Status() int { return http.StatusNotFound }

// Log logs the error and message.
func (e ErrorNotFound) Log() {
	if e.LogFields == nil && e.Err != nil {
		e.LogFields = make(log.Fields)
		e.LogFields["err"] = e.Err.Error()
	}
	log.WithFields(e.LogFields).Warnln(e.LogMsg)
}

// getSession always returns a session and intentionally ignores errors by CookieStore.Get.
// See http://www.gorillatoolkit.org/pkg/sessions#CookieStore.Get
func getSession(env *Env, r *http.Request) *sessions.Session {
	session, _ := env.Store.Get(r, env.Conf.SessionName)
	return session
}

// Handler passes an instance of Env to actions and executes them.
// It also handles errors returned by the actions.
// Actions have to respond by them themselves.
//
// Inspired by http://blog.questionable.services/article/http-handler-error-handling-revisited/.
type Handler struct {
	Env    *Env
	Action func(e *Env, w http.ResponseWriter, r *http.Request) error
}

func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := h.Action(h.Env, w, r)
	if err != nil {
		switch e := err.(type) {
		case services.Error:
			// add error message as flash
			session := getSession(h.Env, r)
			session.AddFlash(err.Error())
			if serr := session.Save(r, w); serr != nil {
				log.WithField("stack", fmt.Sprintf("%+v", errors.WithStack(serr))).Error(serr.Error())
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				return
			}

			// redirect to the same URL
			http.Redirect(w, r, r.URL.Path, http.StatusFound)
		case ErrorNotFound:
			e.Log()
			http.Error(w, e.Error(), e.Status())
		default:
			log.WithField("stack", fmt.Sprintf("%+v", errors.WithStack(e))).Error(e.Error())
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
		return
	}
}
