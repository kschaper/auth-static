package handler

import (
	"fmt"
	"mime"
	"net/http"
	"path"
	"strings"

	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
)

// AuthenticationAction gets the user_id from the session and checks if there's a corresponding user in the database.
func AuthenticationAction(env *Env, w http.ResponseWriter, r *http.Request) error {
	session := getSession(env, r)

	// get user_id from session and convert into UUID
	userID := session.Values[env.Conf.UserIDKey]
	if userID == nil {
		return ErrorNotFound{LogMsg: "could not get user_id from session"}
	}
	userUUID, err := uuid.FromString(fmt.Sprintf("%s", userID))
	if err != nil {
		return ErrorNotFound{Err: err, LogMsg: "invalid user_id in session", LogFields: log.Fields{"user_id": userID}}
	}

	// check if user exists
	exists, err := env.UserService.Exists(userUUID)
	if err != nil {
		return ErrorNotFound{Err: err, LogMsg: "could not check user existence"}
	}
	if !exists {
		return ErrorNotFound{LogMsg: "unknown user", LogFields: log.Fields{"user_id": userUUID}}
	}

	// set Content-Type header
	mime := mime.TypeByExtension(path.Ext(r.URL.Path))
	if mime == "" {
		mime = "text/html"
	}
	w.Header().Set("Content-Type", mime)

	// set header
	value := strings.Replace(r.URL.String(), env.Conf.ProtectedAreaDirExternal, env.Conf.ProtectedAreaDirInternal, 1)
	w.Header().Set("X-Accel-Redirect", value)
	return nil
}
