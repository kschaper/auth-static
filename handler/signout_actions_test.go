package handler_test

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/sessions"
	"gitlab.com/kschaper/auth-static/assert"
	"gitlab.com/kschaper/auth-static/handler"
	"gitlab.com/kschaper/auth-static/services"
)

func TestSignoutAction(t *testing.T) {
	cases := map[string]func(t *testing.T){
		"success": func(t *testing.T) {
			var (
				db          = db(t)
				userService = &services.UserService{DB: db}
				id          = createSignedUpUser(t, userService, "webmaster@example.com", strings.Repeat("k", services.PasswordMinLen))
				conf        = testConfig()
				store       = sessions.NewCookieStore([]byte("abc"))
				env         = &handler.Env{Conf: conf, Store: store, UserService: userService}
				h           = handler.Handler{Env: env, Action: handler.SignoutAction}
				w           = httptest.NewRecorder()
			)

			// request
			req, err := http.NewRequest("POST", "/signout", nil)
			if err != nil {
				t.Fatal(err)
			}

			// put the user id in session
			session, _ := store.Get(req, conf.SessionName)
			session.Values[conf.UserIDKey] = id
			if err := session.Save(req, w); err != nil {
				t.Fatal(err)
			}

			// set the session cookie for the request
			req.Header.Set("Set-Cookie", w.Result().Cookies()[0].Value)

			// invoke handler
			h.ServeHTTP(w, req)

			// ensure status code is correct
			if w.Code != http.StatusFound {
				t.Fatalf("expected status code %d but got %d\n", http.StatusFound, w.Code)
			}

			// ensure redirect to sign-in page
			location := w.Header().Get("Location")
			expectedLocation := "/signin"
			if location != expectedLocation {
				t.Fatalf("expected redirect to %s but was to %s\n", expectedLocation, location)
			}

			// ensure user.ID is not in session
			assert.UserIDNotInSession(t, req, store, conf.SessionName, conf.UserIDKey)
		},
	}

	for n, c := range cases {
		t.Run(n, c)
	}
}
