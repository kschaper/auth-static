package handler

import (
	"fmt"
	"html/template"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/kschaper/auth-static/services"
)

type signupFormTplData struct {
	Code           string   // from URL
	PasswordMinLen int      // from package services
	Errors         []string // from flash messages
}

var signupFormTpl = template.Must(template.New("signup").Parse(`<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>sign up</title>
  </head>
  <body>
		<h1>sign up</h1>
		<p>The password must have at least {{.PasswordMinLen}} characters.</p>
    <form action="/signup/{{.Code}}" method="post">
      password: <input type="password" name="password">
      again: <input type="password" name="confirmation">
      <input type="submit" value="sign up">
		</form>
		{{if .Errors}}
			<ul>
				{{range .Errors}}
					<li>{{.}}</li>
				{{end}}
			</ul>
		{{end}}
  </body>
</html>
`))

// SignupFormAction shows the sign up form.
func SignupFormAction(env *Env, w http.ResponseWriter, r *http.Request) error {
	var (
		session = getSession(env, r)
		vars    = mux.Vars(r)
		code    = vars["code"]
	)

	// template data
	data := signupFormTplData{
		Code:           code,
		PasswordMinLen: services.PasswordMinLen,
	}

	if flashes := session.Flashes(); len(flashes) > 0 {
		for _, flash := range flashes {
			data.Errors = append(data.Errors, fmt.Sprintf("%s", flash))
		}
	}

	if err := session.Save(r, w); err != nil {
		return errors.Wrap(err, "could not save session")
	}

	// show page
	if err := signupFormTpl.Execute(w, data); err != nil {
		return errors.Wrap(err, "could not execute template")
	}
	return nil
}

// SignupAction sets the password and redirects.
func SignupAction(env *Env, w http.ResponseWriter, r *http.Request) error {
	var (
		session      = getSession(env, r)
		vars         = mux.Vars(r)
		code         = vars["code"]
		password     = r.PostFormValue("password")
		confirmation = r.PostFormValue("confirmation")
		id           uuid.UUID
	)

	// get user id
	id, err := env.UserService.GetIDByCode(code)
	if err != nil {
		return err
	}

	// update password
	err = env.UserService.UpdatePassword(id, password, confirmation)
	if err != nil {
		return err
	}

	// store user id in session
	session.Values[env.Conf.UserIDKey] = id.String()
	if err := session.Save(r, w); err != nil {
		return errors.Wrap(err, "could not save session")
	}

	// redirect to protected area
	http.Redirect(w, r, env.Conf.ProtectedAreaDirExternal+env.Conf.ProtectedAreaHome, http.StatusFound)
	return nil
}
