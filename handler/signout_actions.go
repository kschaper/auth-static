package handler

import (
	"net/http"

	"github.com/pkg/errors"
)

// SignoutAction expires the session and redirects to the sign-in form.
func SignoutAction(env *Env, w http.ResponseWriter, r *http.Request) error {
	// destroy session
	session := getSession(env, r)
	delete(session.Values, env.Conf.UserIDKey)
	session.Options.MaxAge = -1
	if err := session.Save(r, w); err != nil {
		return errors.Wrap(err, "could not save session")
	}

	// redirect to sign-in page
	http.Redirect(w, r, "/signin", http.StatusFound)
	return nil
}
