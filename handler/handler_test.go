package handler_test

import (
	"database/sql"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"

	"github.com/gorilla/sessions"
	log "github.com/sirupsen/logrus"
	"gitlab.com/kschaper/auth-static/assert"
	"gitlab.com/kschaper/auth-static/config"
	"gitlab.com/kschaper/auth-static/handler"
	"gitlab.com/kschaper/auth-static/services"
)

func TestHandler(t *testing.T) {
	cases := map[string]func(t *testing.T){
		"service error": func(t *testing.T) {
			var (
				store = sessions.NewCookieStore([]byte("abc"))
				conf  = testConfig()
				env   = &handler.Env{Store: store, Conf: conf}

				action = func(env *handler.Env, w http.ResponseWriter, r *http.Request) error {
					return services.ErrCodeUnknown
				}

				h = handler.Handler{Env: env, Action: action}
				w = httptest.NewRecorder()

				path = "/foo/bar"
			)

			// request
			req, err := http.NewRequest("GET", "http://example.com"+path, nil)
			if err != nil {
				t.Fatal(err)
			}

			// server
			h.ServeHTTP(w, req)
			resp := w.Result()

			assert.StatusCode(t, resp, http.StatusFound)
			assert.ContentType(t, resp, "text/html")

			// ensure error message is set in flash
			sess, _ := store.Get(req, conf.SessionName)
			flashes := sess.Flashes()
			expectedFlashes := []interface{}{services.ErrCodeUnknown.Error()}
			if !reflect.DeepEqual(flashes, expectedFlashes) {
				t.Fatalf("expected flashes to be %#v but got %#v\n", expectedFlashes, flashes)
			}

			// ensure redirect to the same URL
			location := resp.Header.Get("Location")
			if location != path {
				t.Fatalf("expected redirect to %s but was to %s\n", path, location)
			}
		},
		"not found error": func(t *testing.T) {
			var (
				action = func(env *handler.Env, w http.ResponseWriter, r *http.Request) error {
					return handler.ErrorNotFound{
						Err:       errors.New("not found"),
						LogMsg:    "unknown user",
						LogFields: log.Fields{"user_id": "abc"},
					}
				}

				h   = handler.Handler{Action: action}
				w   = httptest.NewRecorder()
				req = httptest.NewRequest("GET", "http://example.com/foo", nil)
			)

			h.ServeHTTP(w, req)
			resp := w.Result()

			assert.StatusCode(t, resp, http.StatusNotFound)
			assert.ContentType(t, resp, "text/plain")
			assert.Content(t, resp, http.StatusText(http.StatusNotFound))
		},
		"undefined error": func(t *testing.T) {
			var (
				action = func(env *handler.Env, w http.ResponseWriter, r *http.Request) error {
					return errors.New("kaputt")
				}

				h   = handler.Handler{Action: action}
				w   = httptest.NewRecorder()
				req = httptest.NewRequest("GET", "http://example.com/foo", nil)
			)

			h.ServeHTTP(w, req)
			resp := w.Result()

			assert.StatusCode(t, resp, http.StatusInternalServerError)
			assert.ContentType(t, resp, "text/plain")
			assert.Content(t, resp, http.StatusText(http.StatusInternalServerError))
		},
		"success": func(t *testing.T) {
			var (
				action = func(env *handler.Env, w http.ResponseWriter, r *http.Request) error {
					w.Header().Set("Content-Type", "text/html")
					io.WriteString(w, http.StatusText(http.StatusOK)+"\n")
					return nil
				}

				h   = handler.Handler{Action: action}
				w   = httptest.NewRecorder()
				req = httptest.NewRequest("GET", "http://example.com/foo", nil)
			)

			h.ServeHTTP(w, req)
			resp := w.Result()

			assert.StatusCode(t, resp, http.StatusOK)
			assert.ContentType(t, resp, "text/html")
			assert.Content(t, resp, http.StatusText(http.StatusOK))
		},
	}

	for n, c := range cases {
		t.Run(n, c)
	}
}

// helpers
// ----------------------------------------------------------------------------

func testConfig() *config.Config {
	return &config.Config{
		SessionName:              "auth-static",
		UserIDKey:                "user_id",
		ProtectedAreaDirExternal: "/private/",
		ProtectedAreaDirInternal: "/internal/",
		ProtectedAreaHome:        "main.html",
	}
}

func db(t *testing.T) *sql.DB {
	client := &services.DatabaseClient{DSN: ":memory:"}
	db, err := client.Open()
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("%s:%d: could not open db: %q\n", filepath.Base(file), line, err)
		t.FailNow()
	}
	return db
}

func getRequest(t *testing.T, uri string) *http.Response {
	resp, err := http.Get(uri)
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("%s:%d: GET request failed: %q\n", filepath.Base(file), line, err)
		t.FailNow()
	}
	return resp
}

func postRequest(t *testing.T, uri string, data url.Values) *http.Response {
	client := &http.Client{
		CheckRedirect: func(*http.Request, []*http.Request) error {
			return http.ErrUseLastResponse // do not follow redirects
		},
	}

	resp, err := client.PostForm(uri, data)
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("%s:%d: POST request failed: %q\n", filepath.Base(file), line, err)
		t.FailNow()
	}
	return resp
}

// createUser creates a user and returns its code.
func createUser(t *testing.T, userService *services.UserService, email string) string {
	code, err := userService.Create(email)
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("%s:%d: could not create user: %q\n", filepath.Base(file), line, err)
		t.FailNow()
	}
	return code
}

// createSignedUpUser creates a user, sets its password and returns the ID converted to string.
func createSignedUpUser(t *testing.T, userService *services.UserService, email, password string) string {
	// create a user
	code := createUser(t, userService, email)

	// get user id
	id, err := userService.GetIDByCode(code)
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("%s:%d: could get user.ID: %q\n", filepath.Base(file), line, err)
		t.FailNow()
	}

	// set password
	if err := userService.UpdatePassword(id, password, password); err != nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("%s:%d: could update password: %q\n", filepath.Base(file), line, err)
		t.FailNow()
	}

	return id.String()
}
