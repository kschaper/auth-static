package handler

import (
	"fmt"
	"html/template"
	"net/http"

	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
)

type signinFormTplData struct {
	Errors []string // from flash messages
}

var signinFormTpl = template.Must(template.New("signin").Parse(`<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>sign in</title>
  </head>
  <body>
		<h1>sign in</h1>
    <form action="/signin" method="post">
      email: <input type="text" name="email">
      password: <input type="password" name="password">
      <input type="submit" value="sign in">
		</form>
		{{if .Errors}}
			<ul>
				{{range .Errors}}
					<li>{{.}}</li>
				{{end}}
			</ul>
		{{end}}
  </body>
</html>
`))

// SigninFormAction shows the sign in form.
func SigninFormAction(env *Env, w http.ResponseWriter, r *http.Request) error {
	session := getSession(env, r)

	// template data
	data := signinFormTplData{}
	if flashes := session.Flashes(); len(flashes) > 0 {
		for _, flash := range flashes {
			data.Errors = append(data.Errors, fmt.Sprintf("%s", flash))
		}
	}

	if err := session.Save(r, w); err != nil {
		return errors.Wrap(err, "could not save session")
	}

	// show page
	if err := signinFormTpl.Execute(w, data); err != nil {
		return errors.Wrap(err, "could not execute template")
	}
	return nil
}

// SigninAction authenticates and redirects to the protected area.
func SigninAction(env *Env, w http.ResponseWriter, r *http.Request) error {
	var (
		session  = getSession(env, r)
		email    = r.PostFormValue("email")
		password = r.PostFormValue("password")
	)

	// get the user
	user, err := env.UserService.GetByEmailAndPassword(email, password)
	if err != nil {
		return err
	}

	// handle authentication failed
	if user.ID == uuid.Nil {
		session.AddFlash("email and/or password wrong")
		if err := session.Save(r, w); err != nil {
			return errors.Wrap(err, "could not save session")
		}
		http.Redirect(w, r, "/signin", http.StatusFound)
		return nil
	}

	// store user id in session
	session.Values[env.Conf.UserIDKey] = user.ID.String()
	if err := session.Save(r, w); err != nil {
		return errors.Wrap(err, "could not save session")
	}

	// redirect to protected area
	http.Redirect(w, r, env.Conf.ProtectedAreaDirExternal+env.Conf.ProtectedAreaHome, http.StatusFound)
	return nil
}
