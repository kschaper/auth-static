package services

import (
	"crypto/rand"
	"database/sql"
	"encoding/hex"
	"fmt"
	"strings"

	sqlite3 "github.com/mattn/go-sqlite3"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

// User represents a user.
type User struct {
	ID    uuid.UUID
	Email string
}

// PasswordMinLen is the minimum password length.
const PasswordMinLen = 8

// Error represents an error returned on expected errors.
type Error string

// Error returns the error message
func (e Error) Error() string {
	return string(e)
}

const (
	// ErrEmailRequired is returned when the given email is empty.
	ErrEmailRequired = Error("email required")
	// ErrPasswordTooShort is returned when the length of the given password is less than PasswordMinLen.
	ErrPasswordTooShort = Error("password too short")
	// ErrPasswordNotConfirmed is return when the given password and its confirmation don't match.
	ErrPasswordNotConfirmed = Error("password and confirmation don't match")
	// ErrCodeUnknown is returned when the given code could not be found.
	ErrCodeUnknown = Error("code unknown")
	// ErrEmailUnknown is returned when the given email could not be found.
	ErrEmailUnknown = Error("email unknown")
	// ErrEmailNotUnique is returned when the email already exists.
	ErrEmailNotUnique = Error("email not unique")
)

// UserService manages users.
type UserService struct {
	DB *sql.DB
}

// Create creates a new user with the given email and a generated code which is then returned.
// The code can be used for the signup URL: `https://example.com/signup/<code>`.
func (service *UserService) Create(email string) (string, error) {
	// validate email length
	if len(email) == 0 {
		return "", ErrEmailRequired
	}

	// generate new code
	code, err := generateCode()
	if err != nil {
		return "", errors.Wrap(err, "could not generate code")
	}

	// create new user
	stmt, err := service.DB.Prepare("INSERT INTO users (id, email, code, created_at) VALUES (?, ?, ?, DATETIME('now'))")
	if err != nil {
		return "", errors.Wrap(err, "could not prepare statement")
	}
	defer closeStatement(stmt)

	// handle email not unique
	_, err = stmt.Exec(uuid.NewV4(), email, code)
	if sqliteErr, ok := err.(sqlite3.Error); ok {
		if sqliteErr.Code == 19 {
			return "", ErrEmailNotUnique
		}
	}
	if err != nil {
		return "", errors.Wrap(err, "could not execute query")
	}

	return code, nil
}

// Reset empties the hashed password and generates a new code for the given email.
func (service *UserService) Reset(email string) (string, error) {
	// generate new code
	code, err := generateCode()
	if err != nil {
		return "", errors.Wrap(err, "could not generate code")
	}

	// update user
	stmt, err := service.DB.Prepare("UPDATE users SET hash = '', code = ?, updated_at = strftime('%Y-%m-%d %H:%M:%f') WHERE email = ?")
	if err != nil {
		return "", errors.Wrap(err, "could not prepare statement")
	}
	defer closeStatement(stmt)

	res, err := stmt.Exec(code, email)
	if err != nil {
		return "", errors.Wrap(err, "could not execute query")
	}

	// check if email is unknown
	rows, err := res.RowsAffected()
	if err != nil {
		return "", errors.Wrap(err, "could not get the number of affected rows")
	}
	if rows == 0 {
		return "", ErrEmailUnknown
	}
	return code, nil
}

// UpdatePassword stores the hashed password and empties the code. This is basically the sign up process.
func (service *UserService) UpdatePassword(id uuid.UUID, password, confirmation string) error {
	password = strings.TrimSpace(password)
	confirmation = strings.TrimSpace(confirmation)

	// validate minimum password length
	if len(password) < PasswordMinLen {
		return ErrPasswordTooShort
	}

	// validate password confirmation
	if password != confirmation {
		return ErrPasswordNotConfirmed
	}

	// generate password hash
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return errors.Wrap(err, "could not generate hash")
	}

	// update user
	stmt, err := service.DB.Prepare("UPDATE users SET hash = ?, code = '', updated_at = DATETIME('now') WHERE id = ?")
	if err != nil {
		return errors.Wrap(err, "could not prepare statement")
	}
	defer closeStatement(stmt)

	if _, err = stmt.Exec(string(hash), id); err != nil {
		return errors.Wrap(err, "could not execute query")
	}
	return nil
}

// GetIDByCode returns the user ID for the given code.
func (service *UserService) GetIDByCode(code string) (uuid.UUID, error) {
	var id string
	err := service.DB.QueryRow("SELECT id FROM users WHERE code = ?", code).Scan(&id)
	if err == sql.ErrNoRows {
		return uuid.Nil, ErrCodeUnknown
	}
	if err != nil {
		return uuid.Nil, errors.Wrap(err, "could not execute query")
	}

	parsedID, err := uuid.FromString(id)
	if err != nil {
		return uuid.Nil, errors.Wrap(err, "could not parse UUID")
	}
	return parsedID, nil
}

// GetIDByEmail returns the user ID for the given email.
func (service *UserService) GetIDByEmail(email string) (uuid.UUID, error) {
	var id string
	err := service.DB.QueryRow("SELECT id FROM users WHERE email = ?", email).Scan(&id)
	if err == sql.ErrNoRows {
		return uuid.Nil, ErrEmailUnknown
	}
	if err != nil {
		return uuid.Nil, errors.Wrap(err, "could not execute query")
	}

	parsedID, err := uuid.FromString(id)
	if err != nil {
		return uuid.Nil, errors.Wrap(err, "could not parse UUID")
	}
	return parsedID, nil
}

// GetByEmailAndPassword returns a signed-up User for the given email and password.
func (service *UserService) GetByEmailAndPassword(email, password string) (*User, error) {
	email = strings.TrimSpace(email)
	password = strings.TrimSpace(password)

	// get id and hash
	var id uuid.UUID
	var hash sql.NullString
	err := service.DB.QueryRow("SELECT id, hash FROM users WHERE email = ?", email).Scan(&id, &hash)
	if err == sql.ErrNoRows || len(hash.String) == 0 {
		return &User{}, nil
	}
	if err != nil {
		return &User{}, errors.Wrap(err, "could not execute query")
	}

	// compare hash and password
	err = bcrypt.CompareHashAndPassword([]byte(hash.String), []byte(password))

	// password wrong
	if err == bcrypt.ErrMismatchedHashAndPassword {
		return &User{}, nil
	}

	// some other error
	if err != nil {
		return &User{}, errors.Wrap(err, "could not compare hash and password")
	}
	return &User{ID: id, Email: email}, nil
}

// Exists checks if the user with the given ID exists.
func (service *UserService) Exists(id uuid.UUID) (bool, error) {
	var count int
	if err := service.DB.QueryRow("SELECT COUNT(id) FROM users WHERE id = ?", id).Scan(&count); err != nil {
		return false, errors.Wrap(err, "could not execute query")
	}
	return count == 1, nil
}

// generateCode generates a code.
func generateCode() (string, error) {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		return "", errors.Wrap(err, "could not generate random number")
	}
	return hex.EncodeToString(b), nil
}

func closeStatement(stmt *sql.Stmt) {
	if err := stmt.Close(); err != nil {
		log.WithField("stack", fmt.Sprintf("%+v", errors.WithStack(err))).Error(err.Error())
	}
}
