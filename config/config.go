package config

import (
	"encoding/hex"

	"github.com/gorilla/securecookie"
)

// GenerateKey generates a random string to be used as cookie hash/block key.
func GenerateKey() string {
	key := securecookie.GenerateRandomKey(16)
	return hex.EncodeToString(key)
}

// Config provides configuration.
type Config struct {
	// SessionName is the cookie name for the session
	SessionName string

	// UserIDKey is the user_id session key
	UserIDKey string

	// ProtectedAreaDirExternal is the URL path of the protected area visible to the user.
	ProtectedAreaDirExternal string
	// ProtectedAreaDirInternal is the URL path of the protected area not visible to the user.
	ProtectedAreaDirInternal string
	// ProtectedAreaHome is the URL of the protected area's homepage.
	ProtectedAreaHome string
}
